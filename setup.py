from setuptools import setup
with open("README.md", "r") as fh:
      long_description = fh.read()
setup(name='octaglobe',
      version='0.1',
      description='Utility to convert a map or a panoramic image to an octaglobe pattern',
      url='http://github.com/Postrediori/OctaGlobe',
      long_description=long_description,
      long_description_content_type="text/markdown",
      author='Rasim Labibov',
      author_email='labibov.rasim@gmail.com',
      packages=['octaglobe'],
      zip_safe=False,
      install_requires=['numpy', 'Pillow'])
