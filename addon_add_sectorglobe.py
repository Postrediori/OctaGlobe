bl_info = {
    "name": "New SectorGlobe",
    "author": "Rasim Labibov",
    "version": (1, 1),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh > New SectorGlobe",
    "description": "Adds a new SectorGlobe",
    "warning": "",
    "doc_url": "",
    "category": "Add Mesh",
}


import bpy
from bpy.types import Operator
from bpy.props import FloatVectorProperty, FloatProperty
from bpy_extras.object_utils import AddObjectHelper, object_data_add
from mathutils import Vector
from math import sqrt


def add_object(self, context):
    scale_x = self.scale.x
    scale_y = self.scale.y
    scale_z = self.scale.z
    
    # Radius
    R = self.radius
    
    # SectorGlobe parameters
    A = R * sqrt(sqrt(2) * (sqrt(2) - 1))
    B = R * 1 / sqrt(2)
    C = R * sqrt(5 / 4 - 1 / sqrt(2))
    D = R * sqrt(3 / 2 - sqrt(2))
    
    # Sides of a square that truncates to an octagon with side B and D
    S = B * (1 + 2 / sqrt(2))
    T = D * (1 + 2 / sqrt(2))
    
    # Halves
    A2 = A / 2
    B2 = B / 2
    D2 = D / 2
    
    S2 = S / 2
    T2 = T / 2
    
    # Height of SectorGlobe
    H2 = sqrt(R**2 - (D2**2 + T2**2))
    H = H2 * 2
    
    # Pre-defined data
    polar_octagon = [
        (D2, -T2), (T2, -D2), (T2, D2), (D2, T2), # Right half
        (-D2, T2), (-T2, D2),  (-T2, -D2), (-D2, -T2), # Left half
    ]
    
    equatorial_octagon = [
        (B2, -S2), (S2, -B2),  (S2, B2), (B2, S2), # Right half
        (-B2, S2), (-S2, B2),  (-S2, -B2), (-B2, -S2), # Left half
    ]

    # Mesh data
    verts = []
    edges = []
    faces = []

    seamed_edges = []
    
    
    #
    # Start mesh generation
    #
    
    # North polar octagon
    for (x, y) in polar_octagon:
        verts.append(Vector((x * scale_x, y * scale_y, H2 * scale_z)))
    k = 0 # start idx of north polar vertices
    faces.append([k, k+1, k+2, k+3, k+4, k+5, k+6, k+7])

    for i in range(7):
        seamed_edges.append([k+i,k+i+1])
    
    # North equatorial octagon
    for (x, y) in equatorial_octagon:
        verts.append(Vector((x * scale_x, y * scale_y, A2 * scale_z)))
         
    # North trapezoid belt
    k = 8 # start idx of north equatorial vertices
    m = 8 # diff with north polar vertices idx
    for i in range(8):
        if i<7:
            a,b,c,d = k+i, k+1+i, k-m+1+i, k-m+i
        else:
            a,b,c,d = k+1+6, k, k-m, k-m+1+6  # Wrap the last trapezoid
        faces.append([a, b, c, d])
        seamed_edges.append([d, a])
    
    # South equatorial octagon
    for (x, y) in equatorial_octagon:
        verts.append(Vector((x * scale_x, y * scale_y, -A2 * scale_z)))
        
    
    # Equatorial squares
    k = 16 # start idx of south equatorial vertices
    m = 8 # diff with north equatorial octagon vertices
    for i in range(7):
        faces.append([k+i, k+1+i, k-m+1+i, k-m+i])
    faces.append([k+1+6, k, k-m, k-m+1+6])
    
    
    # South polar octagon vertices
    for (x, y) in polar_octagon:
        verts.append(Vector((x * scale_x, y * scale_y, -H2 * scale_z)))
    
    
    # South trapezoid belt
    k = 24 # start idx of south polar vertices
    m = 8 # diff with south equatorial octagon vertices
    for i in range(8):
        if i<7:
            a,b,c,d = k+i, k+1+i, k-m+1+i, k-m+i
        else:
            a,b,c,d = k+1+6, k, k-m, k-m+1+6
        faces.append([a, b, c, d])
        seamed_edges.append([d, a])
    
    
    # South polar octagon face
    k = 24 # start idx of south polar vertices
    faces.append([k, k+1, k+2, k+3, k+4, k+5, k+6, k+7])
    
    for i in range(7):
        seamed_edges.append([k+i,k+i+1])

    # North-south seam
    n,s = 8,8  # TODO: adjustable polar octagon connections
    seamed_edges.append([7+n, 15+s])
    
    #
    # Finish mesh generation
    #

    mesh = bpy.data.meshes.new(name="New SectorGlobe")
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    for e in mesh.edges:
        if list(e.vertices) in seamed_edges:
            e.use_seam = True
    object_data_add(context, mesh, operator=self)


class OBJECT_OT_add_object(Operator, AddObjectHelper):
    """Create a new SectorGlobe"""
    bl_idname = "mesh.add_sectorglobe"
    bl_label = "Add SectorGlobe"
    bl_options = {'REGISTER', 'UNDO'}

    scale: FloatVectorProperty(
        name="scale",
        default=(1.0, 1.0, 1.0),
        subtype='TRANSLATION',
        description="scaling",
    )
    
    radius: FloatProperty(
        name="radius",
        default=1.0,
        subtype='DISTANCE',
        description="radius of a sectorglobe"
    )

    def execute(self, context):

        add_object(self, context)

        return {'FINISHED'}


# Registration

def add_object_button(self, context):
    self.layout.operator(
        OBJECT_OT_add_object.bl_idname,
        text="Add SectorGlobe",
        icon='PLUGIN')


# This allows you to right click on a button and link to documentation
def add_object_manual_map():
    url_manual_prefix = "https://docs.blender.org/manual/en/latest/"
    url_manual_mapping = (
        ("bpy.ops.mesh.add_object", "scene_layout/object/types.html"),
    )
    return url_manual_prefix, url_manual_mapping


def register():
    bpy.utils.register_class(OBJECT_OT_add_object)
    bpy.utils.register_manual_map(add_object_manual_map)
    bpy.types.VIEW3D_MT_mesh_add.append(add_object_button)


def unregister():
    bpy.utils.unregister_class(OBJECT_OT_add_object)
    bpy.utils.unregister_manual_map(add_object_manual_map)
    bpy.types.VIEW3D_MT_mesh_add.remove(add_object_button)


if __name__ == "__main__":
    register()
