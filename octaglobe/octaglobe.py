#!/usr/bin/env python3

import math
from numpy import clip
from enum import Enum
from PIL import Image
from multiprocessing import Pool


class Hemisphere(Enum):
    NORTH = 1
    SOUTH = 2
    EQUATOR = 3 # Ugly(?)


class TileType(Enum):
    POLAR = 1
    EQUATORIAL = 2
    TRAPEZOID = 3


class Config(object):
    OCTAGLOBE_SECTORS = 8

    # Ratios between R and dimensions
    A_R = 0.7654  # math.sqrt(math.sqrt(2.0) * math.sqrt(2.0) - 1.0)
    B_R = 0.7071  # 1.0 / math.sqrt(2.0)
    C_R = 0.7368  # math.sqrt(5.0 / 4.0 - 1.0 / math.sqrt(2.0))
    D_R = 0.2929  # math.sqrt(3.0 / 2.0 - math.sqrt(2.0))
    H_R = 0.7070  # D * (1 + math.sqrt(2.0))

    def __init__(self, radius):
        self.r_img = radius
        self.a_img = int(radius * Config.A_R)
        self.b_img = int(radius * Config.B_R)
        self.c_img = int(radius * Config.C_R)
        self.d_img = int(radius * Config.D_R)
        self.h_img = int(radius * Config.H_R)


def convert_spherical_to_map(coord, map_dimensions):
    """
    Convert spherical coordinates (phi, theta) to pixel of equirectangular map image
    """
    (map_w, map_h) = map_dimensions
    (phi, theta) = coord
    map_x = int((phi / 360.0) * map_w) % map_w
    map_y = int(clip((90.0 - theta) / 180.0 * map_h, 0, map_h - 1))
    return (map_x, map_y)


class GenericTileRenderer(object):
    """
    Renderer class for generic tile
    """
    def __init__(self, config, map_img, bg_color, phi0):
        self.config = config
        self.map_img = map_img
        self.bg_color = bg_color
        self.phi0 = phi0
        self.out_size = (0, 0)
        self.out_position = (0, 0)

    @staticmethod
    def convert_to_dimensionless_tile_coord(coord, config):
        """
        Convert image coordinates to dinemsionless tile coordinates
        """
        return (coord[0] / config.r_img, coord[1] / config.r_img)

    def convert_image_coord_to_tile_coord(self, coord, tile_dimensions):
        return (0, 0)

    def is_coord_in_tile(self, coord):
        return False

    def convert_tile_coord_to_spherical_coord(self, coord):
        return (0, 0)

    def render(self):
        """
        Project equirectangular map to a tile
        """
        in_pixels = self.map_img.load()

        (out_w, out_h) = self.out_size
        out_img = Image.new('RGB', self.out_size, self.bg_color)
        out_pixels = out_img.load()

        for out_y in range(out_h):
            for out_x in range(out_w):
                # Get dimensionless coordinates inside tile
                # Inverse Y for trapezoid tiles
                coord = self.convert_image_coord_to_tile_coord((out_x, out_y), (out_w, out_h))
                coord = self.convert_to_dimensionless_tile_coord(coord, self.config)

                # Check if coordinate is inside a tile
                if not self.is_coord_in_tile(coord):
                    continue

                # Convert tile coordinates (xp, yp) to (phi, theta) = (long, lat)
                (phi, theta) = self.convert_tile_coord_to_spherical_coord(coord)

                # Convert spherical coordinates to equirectangular coordinates
                (in_x, in_y) = convert_spherical_to_map((phi + self.phi0, theta), self.map_img.size)

                # Assign color to output image
                color = in_pixels[in_x, in_y]
                out_pixels[out_x, out_y] = color

        return out_img


class PolarOctahedronRenderer(GenericTileRenderer):
    """
    Render equirectangular map to a polar octagon tile for |theta| > 67.5 deg
    """
    def __init__(self, config, map_img, bg_color, phi0, hemisphere, index):
        super().__init__(config, map_img, bg_color, phi0)
        self.hemisphere = hemisphere
        self.index = index
        self.out_size = (config.h_img, config.h_img)

    def convert_image_coord_to_tile_coord(self, coord, tile_dimensions):
        """
        Convert polar tile coordinate to dimensionless coordinates inside polar octagon
        """
        (x, y) = coord
        (w, h) = tile_dimensions
        return (x - w / 2, y - h / 2)

    def is_coord_in_tile(self, coord):
        """
        Test if a point (x, y) lies within polar octahedron
        """
        (x, y) = coord
        size = (Config.H_R + Config.D_R) / 2
        if (x + y) > size:
            return False
        if (x - y) > size:
            return False
        if (-x + y) > size:
            return False
        if (-x - y) > size:
            return False
        return True

    def convert_tile_coord_to_spherical_coord(self, coord):
        """
        Convert polar image coordinates to spherical coordinates
        """
        (xp, yp) = coord
        r = math.sqrt(xp * xp + yp * yp)
        phi_p = math.degrees(math.atan2(yp, xp))
        # Calculate rotation of polar octagon depending on trapezoid it attaches to
        if self.hemisphere == Hemisphere.NORTH:
            phi = -phi_p + (self.index - 6) * 45.0 + 22.5
        elif self.hemisphere == Hemisphere.SOUTH:
            phi = phi_p + (self.index + 3) * 45.0 - 22.5
        phi_0 = (int(phi) // 45) * 45.0 + 22.5
        delta_phi = phi - phi_0

        theta = 90.0 - 45.0 * r * math.cos(math.radians(math.fabs(delta_phi))) / (Config.D_R * (1.0 + math.sqrt(2.0)))
        # Flip latitudes for south pole
        if self.hemisphere == Hemisphere.SOUTH:
            theta = -theta

        return (phi, theta)


class TrapezoidRenderer(GenericTileRenderer):
    """
    Render equirectangular map to a trapezoid tile for 22.5 deg < |theta| <= 67.5 deg
    """
    def __init__(self, config, map_img, bg_color, phi0, hemisphere, index):
        super().__init__(config, map_img, bg_color, phi0)
        self.hemisphere = hemisphere
        self.index = index
        self.out_size = (self.config.b_img, self.config.c_img)

    def convert_image_coord_to_tile_coord(self, coord, tile_dimensions):
        (x, y) = coord
        (w, h) = tile_dimensions
        # Inverse Y for trapezoid tiles
        return (x, h - y)

    def is_coord_in_tile(self, coord):
        """
        Checks if a tile coord lies within a trapezoid
        """
        (x, y) = coord
        k = (Config.B_R - Config.D_R) / (2.0 * Config.C_R)
        if self.hemisphere == Hemisphere.NORTH:
            if y - x / k > 0.0:
                return False
            if y * k + x - Config.B_R > 0.0:
                return False
        elif self.hemisphere == Hemisphere.SOUTH:
            if y + x / k - Config.B_R < 0.0:
                return False
            if -y * k + x - (Config.B_R + Config.D_R) / 2.0 > 0.0:
                return False
        return True

    def convert_tile_coord_to_spherical_coord(self, coord):
        """
        Project tile coord inside trapezoid of n-th sector to spherical coord
        """
        (x, y) = coord

        if self.hemisphere == Hemisphere.NORTH:
            theta = 45.0 * y / Config.C_R + 22.5
        elif self.hemisphere == Hemisphere.SOUTH:
            theta = 45.0 * (y - Config.C_R) / Config.C_R - 22.5

        phi_0 = 45.0 * self.index + 22.5
        p = x - Config.B_R * 0.5
        q = Config.B_R - (Config.B_R - Config.D_R) * (math.fabs(theta) - 22.5) / 45.0
        delta_phi = 45.0 * p / q
        phi = delta_phi + phi_0

        return (phi, theta)


class EquatorialRenderer(GenericTileRenderer):
    """
    Render equirectangular map to a rectangular equatorial tile for |theta| < 22.5 deg
    """
    def __init__(self, config, map_img, bg_color, phi0, index):
        super().__init__(config, map_img, bg_color, phi0)
        self.index = index
        self.out_size = (self.config.b_img, self.config.a_img) 

    def convert_image_coord_to_tile_coord(self, coord, tile_dimensions):
        (x, y) = coord
        (w, h) = tile_dimensions
        # Move Y origin for rectangular tiles
        return (x, y - h / 2)

    def is_coord_in_tile(self, coord):
        # Rectangular tiles are fully rendered
        return True

    def convert_tile_coord_to_spherical_coord(self, coord):
        """
        Convert tile coordinate of a rectangular tile to a spherical coordinate
        """
        (x, y) = coord

        theta = 45.0 * (-y) / Config.A_R
        phi = 45.0 * x / Config.B_R + 45.0 * self.index

        return (phi, theta)


def render_polar_octahedron(config, map_img, bg_color, phi0, hemisphere, index):
    r = PolarOctahedronRenderer(config, map_img, bg_color, phi0, hemisphere, index)
    pos = (config.b_img * index,
        0 if hemisphere == Hemisphere.NORTH else config.h_img + 2 * config.c_img + config.a_img)
    return (r.render(), pos)

def render_equatorial(config, map_img, bg_color, phi0, index):
    r = EquatorialRenderer(config, map_img, bg_color, phi0, index)
    pos = (index * config.b_img, config.h_img + config.c_img)
    return (r.render(), pos)

def render_trapezoid(config, map_img, bg_color, phi0, hemisphere, index):
    r = TrapezoidRenderer(config, map_img, bg_color, phi0, hemisphere, index)
    pos = (index * config.b_img,
        config.h_img if hemisphere == Hemisphere.NORTH else config.h_img + config.c_img + config.a_img)
    return (r.render(), pos)


def render_tile(tile_type, config, map_img, bg_color, phi0, hemisphere, index ):
    if tile_type == TileType.POLAR:
        tile = render_polar_octahedron(config, map_img, bg_color, phi0, hemisphere, index)
        print("Generated {2} Polar Octagon ({0} x {1} image)".format(tile[0].size[0], tile[0].size[1],
            "North" if hemisphere == Hemisphere.NORTH else "South"))
        return tile

    elif tile_type == TileType.EQUATORIAL:
        tile = render_equatorial(config, map_img, bg_color, phi0, index)
        print("Generated Equatorial Rectangle {0} ({1} x {2} image)".format(index, tile[0].size[0], tile[0].size[1]))
        return tile

    elif tile_type == TileType.TRAPEZOID:
        tile = render_trapezoid(config, map_img, bg_color, phi0, hemisphere, index)
        print("Generated {3} Trapezoid {0} ({1} x {2} image)".format(index, tile[0].size[0], tile[0].size[1],
            "North" if hemisphere == Hemisphere.NORTH else "South"))
        return tile

    else:
        raise ValueError('Unknown tile type was specified')


def convert_map_to_octaglobe(args):
    """
    Convert an equirectangular map to an OctaGlobe image
    """

    # Get arguments
    map_file_name = args.file
    output_file_name = args.output
    radius = args.radius
    bg_color = args.background
    north_index = args.north
    south_index = args.south

    phi0 = 0.0 if not args.rotate else (360. / (float(Config.OCTAGLOBE_SECTORS) * 2.))

    agents = args.processors

    # Load source map
    print("Load source map from file {0}".format(map_file_name))
    map_img = Image.open(map_file_name)

    print("Loaded {0} x {1} image".format(map_img.size[0], map_img.size[1]))

    # Set global size parameters
    config = Config(radius)

    # Pre-set tile params
    # Exact order does not matter for params of multiprocessing.Pool
    tile_params = [
        (TileType.POLAR, config, map_img, bg_color, phi0, Hemisphere.NORTH, north_index),
        (TileType.POLAR, config, map_img, bg_color, phi0, Hemisphere.SOUTH, south_index),
    ]
    for n in range(Config.OCTAGLOBE_SECTORS):
        tile_params.append((TileType.EQUATORIAL, config, map_img, bg_color, phi0, Hemisphere.EQUATOR, n))
        tile_params.append((TileType.TRAPEZOID, config, map_img, bg_color, phi0, Hemisphere.NORTH, n))
        tile_params.append((TileType.TRAPEZOID, config, map_img, bg_color, phi0, Hemisphere.SOUTH, n))

    # Start creating tiles
    tiles = []

    with Pool(agents) as p:
        tiles = p.starmap(render_tile, tile_params)

    # Create full image
    projection_img = Image.new('RGB', (config.b_img * Config.OCTAGLOBE_SECTORS, 2 * config.c_img + config.a_img + 2 * config.h_img),
                               bg_color)

    # Assemble main image from tiles
    # TODO: This operation executes in a single-threaded mode. Is it possible to parallelize this?
    for tile_img, pos in tiles:
        projection_img.paste(tile_img, pos)

    print("Wrote pattern to file {0} ({1} x {2} image)".format(output_file_name,
                                                               projection_img.size[0], projection_img.size[1]))
    projection_img.save(output_file_name)
    projection_img.show()


def main():
    import argparse
    from os import cpu_count

    parser = argparse.ArgumentParser(prog='octaglobe', description='''
      Convert a map or a panoramic image to an octaglobe pattern
    ''')
    parser.add_argument('file', type=str, help='source map filename')
    parser.add_argument('-r', '--radius', type=int, default=500,
                        help='scale factor in pixels. output dimensions are 5.6Rx3.6R (defaults to 500)')
    parser.add_argument('-o', '--output', type=str, default='out.png', metavar='<path>',
                        help='filename for rendered pattern (defaults to "out.png")')
    parser.add_argument('-b', '--background', type=str, default='white', metavar='<color>',
                        help='background color in CSS3 format (defaults to "white")')
    parser.add_argument('-n', '--north', type=int, default=7, choices=range(0, 8),
                        help='index of trapezoid north octagon attaches to (defaults to 7)')
    parser.add_argument('-s', '--south', type=int, default=3, choices=range(0, 8),
                        help='index of trapezoid south octagon attaches to (defaults to 3)')
    parser.add_argument('-t', '--rotate', action='store_true',
                        help='rotate longitudes by 22.5 deg, meridians will move from edges to centers of trapezoids')
    parser.add_argument('-p', '--processors', type=int, default=cpu_count(),
                        help='number of processors to use simultaneously (defaults to number of CPUs in the OS configuration)')

    args = parser.parse_args()

    convert_map_to_octaglobe(args)


if __name__ == '__main__':
    main()
