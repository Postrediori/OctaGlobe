# OctaGlobe

## Introduction

`octaglobe.py` is a Python script to convert an equirectangular (cylindrical projection, skysphere) map or a panoramic image into
an OctaGlobe, polyhedral projection described by [Barry A. Klinger](http://mason.gmu.edu/~bklinger/SectorGlobe/sectorglobe.html). 

An OctaGlobe is a polyhedron with two octahedral, eight rectangular and sixteen trapezoidal faces.
Longitude and latitude circles of a map appear as octagons in this projection.

![OctaGlobe polyhedron](images/OctaGlobe.png)


## Installation for Windows

This section will describe how to setup the module on Windows. This is a bare minimum installation that consists of the following:

* [Python 3 for Windows](https://www.python.org/downloads/windows/).
* [Git](https://git-scm.com/download/win) - for downloading the repository.

Git source control is optional since the repository is quite simple and can be downloaded as an archive from GithHub.

This is not the only possible setup for this module on Windows. One may also check out [Anaconda](https://www.anaconda.com/products/individual) instead of vanilla Python 3 and [PyCharm from JetBrains](https://www.jetbrains.com/pycharm/) for convenient script launch and Python module handling. There is also combined [Anaconda/PyCharm package](https://www.jetbrains.com/pycharm/download/other.html) available.


### Environment setup

Install Python 3 and launch a console with `python.exe` and `pip.exe` in `PATH`. Check versions of Python and the PIP utility by specifying the `-V` argument:

```
C:\>python -V
Python 3.8.3

C:\>pip -V
pip 19.2.3 from c:\python\lib\site-packages\pip (python 3.8)
```

Install the _virtualenv_ module:

```
pip install virtualenv
```


### Clone the repository

Clone the repository with Git:

```
git clone https://github.com/Postrediori/OctaGlobe
cd OctaGlobe
```

Alternatively, you may download an archive (either `.zip` or `.tar.gz`) from the repo's GitHub page and unpack it.


### Setup Python environment

Create a directory for the new [virtual environment](https://docs.python.org/3/library/venv.html) for the module using the previously installed _virtualenv_ module. The following command will create the sub-directory `env`:

```
virtualenv env
```

Activate the virtual environment by launching the script from `env\Scripts\activate`. Note that name of the virtual environment `(env)` will appear in the prompt:

```
C:\Projects\OctaGlobe>env\Scripts\activate

(env) C:\Projects\OctaGlobe>
```

You may exit the virtual environment and continue to work with the global Python context (in case you want to continue working with the current command prompt) by launching the deactivation script. Virtual environment name will disappear from the prompt:

```
(env) C:\Projects\OctaGlobe>env\Scripts\deactivate.bat

C:\Projects\OctaGlobe>
```

Check that pip works in the virtual environment with `pip list`. By default, it contains the bare minimum of modules:

```
(env) C:\Projects\OctaGlobe>pip list
Package    Version
---------- ---------
pip        20.1.1
setuptools 46.4.0
wheel      0.34.2

(env) C:\Projects\OctaGlobe>
```


### Build module

Build the OctaGlobe module and install it to the current virtual environment. 

```
python setup.py install
```

This will install module's dependencies and the module itself to the current virtual environment:

```
(env) C:\Projects\OctaGlobe>pip list
Package    Version
---------- ---------
numpy      1.19.0rc2
octaglobe  0.1
pillow     7.1.2
pip        20.1.1
setuptools 46.4.0
wheel      0.34.2

(env) C:\Projects\OctaGlobe>
```


## Usage

After the setup of virtual environment, the script may launched from the command prompt:

```
python -m octaglobe <arguments>
```

Check that the module is working correctly by specifying the `-h` switch that will display information about arguments:

```
(env) C:\Projects\OctaGlobe>python -m octaglobe -h
usage: octaglobe [-h] [-r RADIUS] [-o <path>] [-b <color>] [-n {0,1,2,3,4,5,6,7}] [-s {0,1,2,3,4,5,6,7}] [-t]
                 [-p PROCESSORS]
                 file

Convert a map or a panoramic image to an octaglobe pattern

positional arguments:
  file                  source map filename

optional arguments:
  -h, --help            show this help message and exit
  -r RADIUS, --radius RADIUS
                        scale factor in pixels. output dimensions are 5.6Rx3.6R (defaults to 500)
  -o <path>, --output <path>
                        filename for rendered pattern (defaults to "out.png")
  -b <color>, --background <color>
                        background color in CSS3 format (defaults to "white")
  -n {0,1,2,3,4,5,6,7}, --north {0,1,2,3,4,5,6,7}
                        index of trapezoid north octagon attaches to (defaults to 7)
  -s {0,1,2,3,4,5,6,7}, --south {0,1,2,3,4,5,6,7}
                        index of trapezoid south octagon attaches to (defaults to 3)
  -t, --rotate          rotate longitudes by 22.5 deg, meridians will move from edges to centers of trapezoids
  -p PROCESSORS, --processors PROCESSORS
                        number of processors to use simultaneously (defaults to number of CPUs in the OS
                        configuration)
```


## Examples

The following sector globe patterns were generated using this module.


### Geologic map of Titan 

Source image by [Lopes et al 2019](https://www.nature.com/articles/s41550-019-0917-6).

PDFs for this globe are available at [BaffinSquid's blog](https://baffinsquid.gitlab.io/papercraft/space/2020/01/14/global-geologic-map-of-titan).

```
python -m octaglobe images/Titan_Geology_Input.jpg -n 2 -s 2 -b pink -o images/Titan_Geology_Pattern.png
```
| Input Map | Output OctaGlobe |
| :---: | :----: |
| ![](images/Titan_Geology_Input.jpg) | ![](images/Titan_Geology_Pattern.png) |

![Geologic map of Titan Papercraft](images/Titan_Geology_Papercraft.jpg)


### Seafloor topography

Source image by [Jesse Allen/NASA's Earth Observatory/British Oceanographic Data Centre](https://visibleearth.nasa.gov/images/73963/bathymetry).

```
python -m octaglobe images/Bathymetry_Input.jpg -n 2 -s 2 -b pink -t -o images/Bathymetry_Pattern.png
```
| Input Map | Output OctaGlobe |
| :---: | :----: |
| ![](images/Bathymetry_Input.jpg) | ![](images/Bathymetry_Pattern.png) |

![Seafloor topography Papercraft](images/Bathymetry_Papercraft.jpg)


### Earth in visible light

Source image by [NASA Goddard Space Flight Center Imag/Reto Stöckli](https://visibleearth.nasa.gov/images/57735/the-blue-marble-land-surface-ocean-color-sea-ice-and-clouds/57738l).

```
python -m octaglobe images/Earth_Visible_Input.jpg -n 2 -s 2 -b pink -t -o images/Bathymetry_Pattern.png
```
| Input Map | Output OctaGlobe |
| :---: | :----: |
| ![](images/Earth_Visible_Input.jpg) | ![](images/Earth_Visible_Pattern.png) |

![Earth in visible light Papercraft](images/Earth_Visible_Papercraft.jpg)


### Curiosity rover panoramic image

Note, that this example is a bit odd since panoramic images are meant to be seen on the inside surface of an imaginary sphere with a viewer in the centre. This result represents somewhat 'inverted perspective'.

Source image by [NASA/JPL-Caltech/MSSS](https://mars.nasa.gov/msl/multimedia/panoramas).

```
python -m octaglobe images/Panoram_Curiosity.jpg -n 2 -s 2 -b white -o images/OctaGlobe_Curiosity.png
```
| Input Panorama | Output OctaGlobe |
| :---: | :----: |
| ![](images/Panorama_Curiosity.jpg) | ![](images/OctaGlobe_Curiosity.png) |


## Create SectorGlobe in Blender

Python addon `addon_add_sectorglobe.py` lets you create a proper SectorGlobe object in Blender. The mesh has automatic UV seams to match the octaglobe scheme. However, one might need to adjust connecting edges of polar octagons.

![SectorGlobe in Blender](images/SectorGlobeBlender.png)

![SectorGlobe UV in Blender](images/SectorGlobeUv.png)


## Links
* [Sector Globe home page](http://mason.gmu.edu/~bklinger/SectorGlobe/sectorglobe.html) - the description of the projection and Matlab script.
  * Barry A. Klinger. [Projection for Creating an OctaGlobe](http://mason.gmu.edu/~bklinger/SectorGlobe/howoctaglobe.pdf). 2012.
* Paper globes made with the help of this script by [BaffinSquid](https://baffinsquid.gitlab.io/):
  * [Papercraft global geologic map of Titan](https://baffinsquid.gitlab.io/papercraft/space/2020/01/14/global-geologic-map-of-titan)
  * [Globe of Jupiter's volcanic moon Io](https://baffinsquid.gitlab.io/papercraft/space/2020/03/31/globe-of-jupiters-moon-io)
  * [Asteroid Ryugu geology globe](https://baffinsquid.gitlab.io/papercraft/space/2020/04/01/ryugu-sector-globe) 
